﻿using System;

namespace DelegatesLambdasEventsApp
{
    public delegate int ResultCalc(int a, int b);
    internal class Program
    {
        static void Main(string[] args)
        {
            // basic delegate
            ResultCalc calc;
            calc = Sum;
            Console.WriteLine(calc(3, 4));

            // annonymous delegate
            ResultCalc rc = delegate (int a, int b)
            {
                return a + b;
            };
            Console.WriteLine("Result: " + rc(3, 3));
            Console.ReadKey();
        }

        static int Sum(int n1, int n2)
        {
            return n1 + n2;
        }
    }
}
